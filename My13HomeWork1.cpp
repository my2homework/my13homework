﻿
#include <iostream>
#include <time.h>

int main()
{
    const int size = 5;
    int array[size][size];
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
                array[i][j] = i + j;

            std::cout << array[i][j] << " ";
        }
        std::cout << "\n";
    }
    
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int day = buf.tm_mday;
    int sum = 0;

    for (int d = 0; d < size; d++)
    {
        sum += array[day % size][d];
    }
        std::cout << sum;

}

